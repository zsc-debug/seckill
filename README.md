# seckill

#### 介绍
京东商城秒杀项目

#### 软件架构

一：系统整体架构图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/104727_ddc605c8_8755510.png "(BNGPIREV_T6S$B_HE)FGEO.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/104828_eb52e731_8755510.png "RL27CSVI9PY}B}Y()I~Q[2Q_edit_631084507052659.png")


二：消息队列实现异步化扣减库存业务逻辑图：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1209/223610_0412125a_8755510.png "OLYN0GCPX1NEKF6}N[BDX51.png")


三：削峰限流实现流程图：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1209/223812_a463f3dc_8755510.png "X9XZ2G`Y$5_IT_EU13%GU8N.png")


四：防刷解决方案及常用技术：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/105050_e7b76d69_8755510.jpeg "Screenshot_20211209_233331.jpg")

（1）令牌桶算法（一段时间内可能处理大量的request,服务器在短时间内能够承受巨大的压力。适用于秒杀项目，短时间内有大量的request）

![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/105307_ff2f1876_8755510.jpeg "Screenshot_20211209_233930.jpg")
  
（2）漏桶算法（一段时间内处理定量的request,服务器承受的压力一直比较稳定。）

![输入图片说明](https://images.gitee.com/uploads/images/2021/1210/105319_8364bf12_8755510.jpeg "Screenshot_20211209_234315_com.nowcoder.app.flori.jpg")